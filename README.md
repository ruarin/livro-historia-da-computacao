# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. CAPÍTULO 1 - CRIAÇÃO
1. CAPÍTULO 2 – PLACAS DE VIDEOS NOS DIAS ATUAIS
1. CAPÍTULO 3 – IMPORTÂNCIA DA ESCOLHA 





## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/11784867/avatar.png?width=400)  | Guilherme Ruaro | ruaro | [guilhermeruaro@alunos.utfpr.edu.br](mailto:jeferson.lima@utfpr.edu.br)

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/11784138/avatar.png?width=400)  | Bianca Maria Antunes | Bibs | [biancaantunes@alunos.utfpr.edu.br](mailto:jeferson.lima@utfpr.edu.br)
